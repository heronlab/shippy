// shippy-service-consignment/main.go
// trigger build [2020-01-21 1579596128]
package main

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/micro/go-micro"
	pb "gitlab.com/phuonghuynhlab/shippy/shippy-service-consignment/proto/consignment"
	pbVessel "gitlab.com/phuonghuynhlab/shippy/shippy-service-vessel/proto/vessel"
)

type repository interface {
	Create(*pb.Consignment) (*pb.Consignment, error)
	GetAll() []*pb.Consignment
}

// consignmentRepositoryImpl - Dummy repository, this simulates the use of a datastore
// of some kind. We'll replace this with a real implementation later on.
type repositoryImpl struct {
	mu           sync.RWMutex
	consignments []*pb.Consignment
}

func (repo *repositoryImpl) Create(consignment *pb.Consignment) (*pb.Consignment, error) {
	repo.mu.Lock()
	updated := append(repo.consignments, consignment)
	repo.consignments = updated
	repo.mu.Unlock()
	return consignment, nil
}

func (repo *repositoryImpl) GetAll() []*pb.Consignment {
	return repo.consignments
}

// Service should implement all of the methods to satisfy the service
// we defined in our protobuf definition. You can check the interface
// in the generated code ifself for the exact method signatures etc
// to give you a better idea.
type service struct {
	repo         repository
	vesselClient pbVessel.VesselServiceClient
}

// CreateConsignment - we created just one method on our service,
// which is a create method, which takes a context and a request as arguments,
// these are handled by the gRPC server.
func (s *service) CreateConsignment(ctx context.Context, req *pb.Consignment, resp *pb.Response) error {
	// find a valid vessel
	vesselResponse, err := s.vesselClient.FindAvailable(ctx, &pbVessel.Specification{
		MaxWeight: req.Weight,
		Capacity:  int32(len(req.Containers)),
	})
	if err != nil {
		return err
	}
	log.Printf("found vessel: %s\n", vesselResponse.Vessel.Id)
	req.VesselId = vesselResponse.Vessel.Id

	consignment, err := s.repo.Create(req)
	if err != nil {
		return err
	}

	// return matching the `Response` message we create in
	// our protobuf definition.
	// resp = &pb.Response{Created: true, Consignment: consignment}
	resp.Created = true
	resp.Consignment = consignment

	return nil
}

func (s *service) GetConsignments(ctx context.Context, req *pb.GetRequest, resp *pb.Response) error {
	consignments := s.repo.GetAll()
	// resp = &pb.Response{Consignments: consignments}
	resp.Consignments = consignments
	return nil
}

func main() {
	repo := new(repositoryImpl)

	// create a new service. Optionally include some options here.
	srv := micro.NewService(
		// this name must match the package name given in your protobuf definition
		micro.Name("shippy.service.consignment"),
	)

	// init will parse the command line flags.
	srv.Init()

	vesselClient := pbVessel.NewVesselServiceClient("shippy.service.vessel", srv.Client())
	svc := &service{repo, vesselClient}

	pb.RegisterConsignmentServiceHandler(srv.Server(), svc)

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
