FROM golang:1.13-alpine

ARG BUILD_PACKAGE

ADD . /src
RUN cd /src \
  && go mod download \
  && CGO_ENABLED=0 go build -o /app ${BUILD_PACKAGE}

# Runtime image
FROM alpine:latest

RUN apk --no-cache add ca-certificates

COPY --from=0 /app /app

CMD ["/app"]
