// shippy-service-vessel/main.go
package main

import (
	"context"
	"errors"
	"fmt"

	micro "github.com/micro/go-micro"
	pb "gitlab.com/phuonghuynhlab/shippy/shippy-service-vessel/proto/vessel"
)

type repository interface {
	FindAvailable(*pb.Specification) (*pb.Vessel, error)
}

type repositoryImpl struct {
	vessels []*pb.Vessel
}

func (repo *repositoryImpl) FindAvailable(spec *pb.Specification) (*pb.Vessel, error) {
	for _, vessel := range repo.vessels {
		if spec.Capacity <= vessel.Capacity && spec.MaxWeight <= vessel.MaxWeight {
			return vessel, nil
		}
	}
	return nil, errors.New("no vessel found")
}

type serviceHandler struct {
	repo repository
}

func (s *serviceHandler) FindAvailable(ctx context.Context, req *pb.Specification, res *pb.Response) error {
	vessel, err := s.repo.FindAvailable(req)
	if err != nil {
		return err
	}

	res.Vessel = vessel
	return nil
}

func main() {
	vessels := []*pb.Vessel{
		&pb.Vessel{Id: "vessel001", Capacity: 100, MaxWeight: 2000, Name: "Boaty 1"},
	}
	repo := &repositoryImpl{vessels}

	service := micro.NewService(micro.Name("shippy.service.vessel"))
	service.Init()

	handler := &serviceHandler{repo}
	pb.RegisterVesselServiceHandler(service.Server(), handler)
	if err := service.Run(); err != nil {
		fmt.Println(err)
	}
}
