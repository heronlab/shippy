// shippy-cli-consignment/main.go
// trigger build [2020-01-22 1579705506]
package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	micro "github.com/micro/go-micro"
	pb "gitlab.com/phuonghuynhlab/shippy/shippy-service-consignment/proto/consignment"
)

const defaultFilename = "consignment.json"

func parseFile(filename string) (*pb.Consignment, error) {
	var consignment *pb.Consignment
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &consignment)
	return consignment, err
}

func main() {
	service := micro.NewService(
		micro.Name("shippy.cli.consignment"),
	)
	service.Init()

	client := pb.NewConsignmentService("shippy.service.consignment", service.Client())
	file := defaultFilename
	if len(os.Args) > 1 {
		file = os.Args[1]
	}
	consignment, err := parseFile(file)
	if err != nil {
		log.Fatalf("could not parse file: %v", err)
	}

	r, err := client.CreateConsignment(context.Background(), consignment)
	if err != nil {
		log.Fatalf("could not create a consignment: %v", err)
	}
	log.Printf("created: %t", r.Created)

	getAll, err := client.GetConsignments(context.Background(), new(pb.GetRequest))
	if err != nil {
		log.Fatalf("could not list consignments: %v", err)
	}
	for _, v := range getAll.Consignments {
		log.Println(v)
	}
}
